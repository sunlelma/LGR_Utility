@echo This will delete .spec, dist/ and build/ then generate the distributable program in dist/
@pause
rmdir dist /s /q
rmdir build /s /q
del LGR_Utility.spec
del LGR_Booster_Patch.spec
del LGR_Booster_Toggle.spec

pyinstaller --onefile LGR_Utility.py
pyinstaller --onefile LGR_Booster_Patch.py
pyinstaller --onefile LGR_Booster_Toggle.py

copy /y LGR_Utility.txt dist\readme.txt

@pause