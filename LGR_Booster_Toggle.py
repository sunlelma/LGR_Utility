from elma.lgr import unpack_LGR
from elma.lgr import pack_LGR
from elma.lgr import LGR_Image
from elma.lgr import LGR
from elma.constants import LGR_DEFAULT_PALETTE
from PIL import Image
from PIL import ImageDraw
import tkinter as tk
import tkinter.filedialog as filedialog
import os
import shutil

def Fancyboost(file):
    my_lgr=unpack_LGR(file)
    for obj in my_lgr.images:
        if(obj.name=="zz000000"):
            print("This lgr is already FancyBOOSTed")
            return
    folder=folder=os.path.dirname(file)+"/backup"
    if not(os.path.exists(folder)):
        os.makedirs(folder)
    shutil.copy(file,folder+"/"+os.path.basename(file))
    imgt=Image.new("P",(200,201),0)  #handle special case palette 0
    imgt.putpalette(my_lgr.palette)
    draw=ImageDraw.Draw(imgt)
    draw.line([0,200,200,200],fill=1,width=1)
    del draw
    my_lgr.images.append(LGR_Image("zz%0.2X%0.2X%0.2X"%(LGR_DEFAULT_PALETTE[0],LGR_DEFAULT_PALETTE[1],LGR_DEFAULT_PALETTE[2]),imgt,LGR_Image.PICTURE,999,LGR_Image.CLIPPING_G,LGR_Image.TRANSPARENCY_BOTTOMLEFT))
    for i in range(1,256):
        imgt=Image.new("P",(200,200),i)
        imgt.putpalette(my_lgr.palette)
        my_lgr.images.append(LGR_Image("zz%0.2X%0.2X%0.2X"%(LGR_DEFAULT_PALETTE[i*3],LGR_DEFAULT_PALETTE[i*3+1],LGR_DEFAULT_PALETTE[i*3+2]),imgt,LGR_Image.PICTURE,999,LGR_Image.CLIPPING_G,LGR_Image.TRANSPARENCY_PAL_ZERO))
    with open(file,"wb") as f:
        f.write(pack_LGR(my_lgr))
    return
    
def UnboostDo(file):
    my_lgr=unpack_LGR(file)
    boosted=False
    for obj in my_lgr.images:
        if(obj.name=="zz000000"):
            boosted=True
    if not boosted:
        print("This lgr is not FancyBOOSTed")
        return
    folder=folder=os.path.dirname(file)+"/backup_boosted"
    if not(os.path.exists(folder)):
        os.makedirs(folder)
    shutil.copy(file,folder+"/"+os.path.basename(file))
    del_list=[]
    for i in range(len(my_lgr.images)):
        if(my_lgr.images[i].name[0:2]=="zz" and len(my_lgr.images[i].name)==8):
            del_list.append(i)
    del_list.reverse()
    for i in del_list:
        del my_lgr.images[i]
    with open(file,"wb") as f:
        f.write(pack_LGR(my_lgr))
    return

class Application(tk.Frame):
    def __init__(self,master):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        
    def create_widgets(self):
        self.frame1=tk.Frame(self)
        self.frame1.pack(side="top")
        self.button1=tk.Button(self.frame1,text="Choose .lgr file",command=self.SelectPopup1)
        self.button1.pack(side="left")
        self.button2=tk.Button(self.frame1,text="Choose folder",command=self.SelectPopup2)
        self.button2.pack(side="left")
        self.entry4=tk.Entry(self,width=80)
        self.entry4.insert(0,"file or directory here")
        self.entry4.pack(side="top")
        self.label8=tk.Label(self,text=" ")
        self.label8.pack(side="top")
        
        self.button6=tk.Button(self,text="***UnBOOST (.lgr/folder of lgrs)***",command=self.Unboost,fg="red")
        self.button6.pack(side="top")
        self.button5=tk.Button(self,text="***FancyBOOST (.lgr/folder of lgrs)***",command=self.Boost,fg="red")
        self.button5.pack(side="top")
        
    def SelectPopup1(self):
        imgfile=filedialog.askopenfilename(filetypes=[("LGR Files","*.lgr"),("All","*")])
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
    def SelectPopup2(self):
        imgfile=filedialog.askdirectory()
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
        
    def Boost(self):
        file=self.entry4.get()
        if(file[-4:].lower()==".lgr"):
            print("FancyBOOSTing 1 lgr file: %s"%(file))
            Fancyboost(file)
        elif(file[-4:].lower()==".txt"):
            return
        else:
            print("Opening folder %s and FancyBOOSTing all .lgr files"%(file))
            for lgrf in os.listdir(file):
                if(lgrf.lower().endswith(".lgr")):
                    print("Processing: %s"%(lgrf))
                    Fancyboost(file+"/"+lgrf)
        print("Done!\n\n\n")
        return
    def Unboost(self):
        file=self.entry4.get()
        if(file[-4:].lower()==".lgr"):
            print("UnBOOSTing 1 lgr file: %s"%(file))
            UnboostDo(file)
        elif(file[-4:].lower()==".txt"):
            return
        else:
            print("Opening folder %s and unBOOSTing all .lgr files"%(file))
            for lgrf in os.listdir(file):
                if(lgrf.lower().endswith(".lgr")):
                    print("Processing: %s"%(lgrf))
                    UnboostDo(file+"/"+lgrf)
        print("Done!\n\n\n")
        return

        
root=tk.Tk()
root.title("sunl's LGR Utility")
app=Application(master=root)

app.mainloop()