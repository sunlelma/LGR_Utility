from elma.lgr import unpack_LGR
from elma.lgr import pack_LGR
from elma.lgr import LGR_Image
from elma.lgr import LGR
from elma.error import check_LGR_error
from elma.constants import LGR_DEFAULT_PALETTE
from elma.constants import LGR_FOOD_NAME
from elma.constants import LGR_NOT_IN_PICTURES_LST
from PIL import Image
from PIL import ImageDraw
import tkinter as tk
import tkinter.filedialog as filedialog
import os
import ctypes
import sys
import pandas
import shutil

image_type_d={LGR_Image.MASK:"MASK",LGR_Image.PICTURE:"PICTURE",LGR_Image.TEXTURE:"TEXTURE"}
undo_image_type_d={image_type_d[k] : k for k in image_type_d}	#reversed dictionary
default_clipping_d={LGR_Image.CLIPPING_U:"U",LGR_Image.CLIPPING_S:"S",LGR_Image.CLIPPING_G:"G"}
undo_default_clipping_d={default_clipping_d[k] : k for k in default_clipping_d}	#reversed dictionary
transparency_d={LGR_Image.TRANSPARENCY_PAL_ZERO:"#000000",LGR_Image.TRANSPARENCY_TOPLEFT:"TOPLEFT",LGR_Image.TRANSPARENCY_TOPRIGHT:"TOPRIGHT",LGR_Image.TRANSPARENCY_BOTTOMLEFT:"BOTTOMLEFT",LGR_Image.TRANSPARENCY_BOTTOMRIGHT:"BOTTOMRIGHT"}
undo_transparency_d={transparency_d[k] : k for k in transparency_d}	#reversed dictionary
default_list=["q1body","q2body","q1thigh","q2thigh","q1leg","q2leg","q1forarm","q2forarm","q1bike","q2bike","q1susp1","q2susp1","q1head","q2head","q1up_arm","q2up_arm","q1susp2","q2susp2","q1wheel","q2wheel","qflag","qexit","qkiller","qcolors","qframe","qgrass","qupdown_folder","cliff","stone1","stone2","stone3","st3top","brick","qfood1","qfood2","bridge","sky","tree2","bush3","tree4","tree5","log2","sedge","tree3","plantain","bush1","bush2","ground","flag","secret","hang","edge","mushroom","log1","tree1","maskbig","maskhor","masklitt","barrel","supphred","suppvred","support2","support3","support1","suspdown","suspup","susp"]

def Fancyboost(file):
    my_lgr=unpack_LGR(file)
    for obj in my_lgr.images:
        if(obj.name=="zz000000"):
            print("This lgr is already FancyBOOSTed")
            return
    folder=folder=os.path.dirname(file)+"/backup"
    if not(os.path.exists(folder)):
        os.makedirs(folder)
    shutil.copy(file,folder+"/"+os.path.basename(file))
    imgt=Image.new("P",(200,201),0)  #handle special case palette 0
    imgt.putpalette(my_lgr.palette)
    draw=ImageDraw.Draw(imgt)
    draw.line([0,200,200,200],fill=1,width=1)
    del draw
    my_lgr.images.append(LGR_Image("zz%0.2X%0.2X%0.2X"%(LGR_DEFAULT_PALETTE[0],LGR_DEFAULT_PALETTE[1],LGR_DEFAULT_PALETTE[2]),imgt,LGR_Image.PICTURE,999,LGR_Image.CLIPPING_G,LGR_Image.TRANSPARENCY_BOTTOMLEFT))
    for i in range(1,256):  #always name according to default palette to be consistent between lgrs
        imgt=Image.new("P",(200,200),i)
        imgt.putpalette(my_lgr.palette)
        my_lgr.images.append(LGR_Image("zz%0.2X%0.2X%0.2X"%(LGR_DEFAULT_PALETTE[i*3],LGR_DEFAULT_PALETTE[i*3+1],LGR_DEFAULT_PALETTE[i*3+2]),imgt,LGR_Image.PICTURE,999,LGR_Image.CLIPPING_G,LGR_Image.TRANSPARENCY_PAL_ZERO))
    with open(file,"wb") as f:
        f.write(pack_LGR(my_lgr))
    return
    
def unzip_LGR(file,default_list):
    my_lgr=unpack_LGR(file)
    lgr_name=os.path.basename(file)[:-4]
    def_palette=False
    has_qupdown=False
    if(my_lgr.palette==LGR_DEFAULT_PALETTE):
        def_palette=True
    else:
        lgr_name=lgr_name+"_X"
    if not(os.path.exists("LGR_unzip/"+lgr_name)):
        os.makedirs("LGR_unzip/"+lgr_name)

    with open("LGR_unzip/"+lgr_name+"/lgr.txt", 'w') as txt:
        txt.write("__comment;sunl's LGR Utility. This lgr "+["does not use default palette (_X added to indicate)","uses default palette"][def_palette]+"\nlgr_filename;"+lgr_name+"\npalette_image;Q1BIKE\nimplement_HQ;yes\n")
        for obj in my_lgr.images:
            if(obj.name[0:2]=="zz"):    #skip boost images
                continue
            if(obj.is_in_pictures_lst()):
                if(obj.is_qup_qdown()):
                    obj.put_palette(my_lgr.palette)
                    if not(os.path.exists("LGR_unzip/"+lgr_name+"/QUPDOWN")):
                        os.makedirs("LGR_unzip/"+lgr_name+"/QUPDOWN")
                    obj.img.save("LGR_unzip/"+lgr_name+"/QUPDOWN/"+obj.name+".bmp","bmp")
                    if(not(has_qupdown)):
                        has_qupdown=True
                        txt.write("QUPDOWN_folder;QUPDOWN\n")
                    if not(os.path.exists("LGR_by_pic/QUPDOWN/"+lgr_name)):
                        os.makedirs("LGR_by_pic/QUPDOWN/"+lgr_name)
                    obj.img.save("LGR_by_pic/QUPDOWN/"+lgr_name+"/"+obj.name+".bmp","bmp")
                elif(obj.is_food()):
                    obj.put_palette(my_lgr.palette)
                    obj.img.save("LGR_unzip/"+lgr_name+"/"+obj.name+".bmp","bmp")
                    txt.write(obj.name+";"+obj.name+".bmp\n")
                    if not(os.path.exists("LGR_by_pic/food")):
                        os.makedirs("LGR_by_pic/food")
                    obj.img.save("LGR_by_pic/food/"+lgr_name+"_"+obj.name[5]+".bmp","bmp")
                else:
                    extra=""
                    if(not(obj.name.lower() in default_list)):
                        extra="_NON_DEFAULT_IMAGES/"
                    if(not(obj.image_type==LGR_Image.MASK)):
                        obj.put_palette(my_lgr.palette)
                    obj.img.save("LGR_unzip/"+lgr_name+"/"+obj.name+".bmp","bmp")
                    try:
                        imgt=image_type_d[obj.image_type]
                    except:
                        imgt="?"
                    try:
                        clipt=default_clipping_d[obj.default_clipping]
                    except:
                        clipt="?"
                    try:
                        transt=transparency_d[obj.transparency]
                    except:
                        transt="?"
                    txt.write(obj.name+";"+obj.name+".bmp;"+imgt+";"+str(obj.default_distance)+";"+clipt+";"+transt+"\n")
                    if not(os.path.exists("LGR_by_pic/"+extra+obj.name)):
                        os.makedirs("LGR_by_pic/"+extra+obj.name)
                    obj.img.save("LGR_by_pic/"+extra+obj.name+"/"+lgr_name+".bmp","bmp")
            else:
                numb=obj.name[1]
                obj.put_palette(my_lgr.palette)
                obj.img.save("LGR_unzip/"+lgr_name+"/"+obj.name+".bmp","bmp")
                txt.write(obj.name+";"+obj.name+".bmp\n")
                if(numb=="1" or numb=="2"):
                    shortname=obj.name[0]+obj.name[2:]
                    if not(os.path.exists("LGR_by_pic/"+shortname)):
                        os.makedirs("LGR_by_pic/"+shortname)
                    obj.img.save("LGR_by_pic/"+shortname+"/"+lgr_name+"_"+numb+".bmp","bmp")
                else:
                    if not(os.path.exists("LGR_by_pic/"+obj.name)):
                        os.makedirs("LGR_by_pic/"+obj.name)
                    obj.img.save("LGR_by_pic/"+obj.name+"/"+lgr_name+".bmp","bmp")
    return

def zip_LGR(pathh):
    #filename=os.path.basename(pathh)
    folder=os.path.dirname(pathh)
    print("------------------------\nBuilding LGR file from folder: "+folder+"\n------------------------")
    columns=[0,1,2,3,4,5,6,7,8,9,10]
    data=pandas.read_csv(pathh,";",engine='python',names=columns)       #python engine and random columns required in order to read csv file of variable width (fill with NaNs)
    my_lgr=LGR()
    use_palette="q1bike"
    use_HQ=True
    lgr_name="my_lgr"
    for i in range(len(data[0])):
        i_name=str(data[0][i])
        i_lower=i_name.lower()
        if(i_name[0:2]=="zz"):
            print('Detected name %s starting with "zz". Name may not start with "zz". Skipping.'%(i_name))
            continue
        elif(i_lower=="__comment"):
            continue
        elif(i_lower=="palette_image"):
            pal_lower=data[1][i].lower()
            if(pal_lower=="default_palette"):
                print("Using default palette")
                use_palette=False
                my_lgr.palette=LGR_DEFAULT_PALETTE
            else:
                use_palette=data[1][i].lower()
        elif(i_lower=="lgr_filename"):
            lgr_name=str(data[1][i])
        elif(i_lower=="implement_hq"):
            if(data[1][i].lower()=="no"):
                use_HQ=False
        elif(i_lower=="qupdown_folder"):
            qupdown_path=folder+"/"+str(data[1][i])
            for item in os.listdir(qupdown_path):
                my_lgr.images.append(LGR_Image(item[:-4],Image.open(qupdown_path+"/"+item)))
        else:
            if(not((i_lower in LGR_FOOD_NAME) or (i_lower in LGR_NOT_IN_PICTURES_LST))):
                try:
                    img_type=undo_image_type_d[data[2][i]]
                except:
                    print("%s unknown image type, defaulting to PICTURE"%i_name)
                    img_type=LGR_Image.PICTURE
                try:
                    dist=int(data[3][i])
                except:
                    print("%s unknown distance, defaulting to 400"%i_name)
                    dist=400
                try:
                    def_clip=undo_default_clipping_d[data[4][i]]
                except:
                    print("%s unknown clipping, defaulting to S"%i_name)
                    def_clip=LGR_Image.CLIPPING_S
                try:
                    trans=undo_transparency_d[data[5][i]]
                except:
                    print("%s unknown transparency, defaulting to TOPLEFT"%i_name)
                    trans=LGR_Image.TRANSPARENCY_TOPLEFT
            else:
                img_type=LGR_Image.PICTURE
                dist=400
                def_clip=LGR_Image.CLIPPING_S
                trans=LGR_Image.TRANSPARENCY_TOPLEFT
            try:
                imgt_path=folder+"/"+str(data[1][i])
                imgt=Image.open(imgt_path)
            except:
                print("Unable to load image of %s: %s)"%(i_name,imgt_path))
                print("Aborting.../n/n/n")
                return
            my_lgr.images.append(LGR_Image(i_name,imgt,img_type,dist,def_clip,trans))
    if(use_palette):
        found_palette=False
        for obj in my_lgr.images:
            if(obj.name.lower()==use_palette):
                if(obj.is_valid_palette_image()):
                    print("Using palette of %s"%(obj.name))
                    found_palette=True
                    my_lgr.palette=obj.get_palette()
                else:
                    print("%s has an invalid palette. Using default palette instead."%(obj.name))
                    my_lgr.palette=LGR_DEFAULT_PALETTE
        if(not(found_palette) and my_lgr.images):
            print("Unable to find indicated palette. Using "+my_lgr.images[0].name+"'s palette")
            if(my_lgr.images[0].is_valid_palette_image()):
                my_lgr.palette=my_lgr.images[0].get_palette()
            else:
                print("%s has an invalid palette. Using default palette instead."%(obj.name))
                my_lgr.palette=LGR_DEFAULT_PALETTE
    for obj in my_lgr.images:
        if(not(obj.is_in_pictures_lst() and obj.image_type==LGR_Image.MASK)):
            tmp=LGR_Image(obj.name,obj.img.copy(),obj.image_type,obj.default_distance,obj.default_clipping,obj.transparency,obj.padding)
            tmp.convert_palette_image(my_lgr.palette,False)
            if(not(obj==tmp)):
                print(obj.name+"'s image file successfully converted into proper palette/format")
                obj.img=tmp.img
        else:
            if(obj.img.mode!="P"):
                obj.convert_palette_image(my_lgr.palette,False)
                print(obj.name+"'s image file successfully converted into palette form")
            
    if(use_HQ==True):
        print("Adding FancyBOOST to lgr")   #Takes 338432 bytes if 75x75 +134400 if 150x150
        imgt=Image.new("P",(200,201),0)  #handle special case palette 0
        imgt.putpalette(my_lgr.palette)
        draw=ImageDraw.Draw(imgt)
        draw.line([0,200,200,200],fill=1,width=1)
        del draw
        my_lgr.images.append(LGR_Image("zz%0.2X%0.2X%0.2X"%(LGR_DEFAULT_PALETTE[0],LGR_DEFAULT_PALETTE[1],LGR_DEFAULT_PALETTE[2]),imgt,LGR_Image.PICTURE,999,LGR_Image.CLIPPING_G,LGR_Image.TRANSPARENCY_BOTTOMLEFT))
        for i in range(1,256):  #always name according to default palette to be consistent between lgrs
            imgt=Image.new("P",(200,200),i)
            imgt.putpalette(my_lgr.palette)
            my_lgr.images.append(LGR_Image("zz%0.2X%0.2X%0.2X"%(LGR_DEFAULT_PALETTE[i*3],LGR_DEFAULT_PALETTE[i*3+1],LGR_DEFAULT_PALETTE[i*3+2]),imgt,LGR_Image.PICTURE,999,LGR_Image.CLIPPING_G,LGR_Image.TRANSPARENCY_PAL_ZERO))
    errors=check_LGR_error(my_lgr)
    if(errors):
        print('------------------------\nThe produced LGR file has the following warnings/errors. These will cause an invalid .lgr file in-game unless otherwise specified or unless marked as a "warning":\n------------------------')
    for i in range(len(errors)):
        print(errors[i][2])
    with open(lgr_name+".lgr","wb") as f:
        f.write(pack_LGR(my_lgr))
    print("------------------------\nLGR file successfully created, saved as %s.lgr\n\n\n"%(lgr_name))
    return

class Application(tk.Frame):
    def __init__(self,master):
        super().__init__(master)
        self.pack()
        self.create_widgets()
        
    def create_widgets(self):
        self.frame1=tk.Frame(self)
        self.frame1.pack(side="top")
        self.button1=tk.Button(self.frame1,text="Choose .lgr file",command=self.SelectPopup1)
        self.button1.pack(side="left")
        self.button2=tk.Button(self.frame1,text="Choose folder",command=self.SelectPopup2)
        self.button2.pack(side="left")
        self.button3=tk.Button(self.frame1,text="Choose .txt file",command=self.SelectPopup3)
        self.button3.pack(side="left")
        self.entry4=tk.Entry(self,width=80)
        self.entry4.insert(0,"file or directory here")
        self.entry4.pack(side="top")
        self.label8=tk.Label(self,text=" ")
        self.label8.pack(side="top")
        
        self.button5=tk.Button(self,text="***FancyBOOST (.lgr/folder of lgrs)***",command=self.Boost,fg="red")
        self.button5.pack(side="top")
        self.button6=tk.Button(self,text="***Unzip (.lgr/folder of lgrs)***",command=self.OpenStuff,fg="red")
        self.button6.pack(side="top")
        self.button7=tk.Button(self,text="***Zip 1 .txt into .lgr***",command=self.CloseStuff,fg="red")
        self.button7.pack(side="top")
        
    def SelectPopup1(self):
        imgfile=filedialog.askopenfilename(filetypes=[("LGR Files","*.lgr"),("All","*")])
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
    def SelectPopup2(self):
        imgfile=filedialog.askdirectory()
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
    def SelectPopup3(self):
        imgfile=filedialog.askopenfilename(filetypes=[("LGR .txt file","*.txt"),("All","*")])
        self.entry4.delete(0,len(self.entry4.get()))
        self.entry4.insert(0,imgfile)
        
    def SelectSavePopup(self):
        savename=filedialog.asksaveasfilename(filetypes=[("Elastomania Level File","*.lev"),("All","*")])
        if(savename[-4:]!=".lev"):
            savename=savename+".lev"
        self.label2.delete(0,len(self.label2.get()))
        self.label2.insert(0,savename)
        
    def Boost(self):
        file=self.entry4.get()
        if(file[-4:].lower()==".lgr"):
            print("FancyBOOSTing 1 lgr file: %s"%(file))
            Fancyboost(file)
        elif(file[-4:].lower()==".txt"):
            return
        else:
            print("Opening folder %s and FancyBOOSTing all .lgr files"%(file))
            for lgrf in os.listdir(file):
                if(lgrf.lower().endswith(".lgr")):
                    print("Processing: %s"%(lgrf))
                    Fancyboost(file+"/"+lgrf)
        print("Done!\n\n\n")
        return
    def OpenStuff(self):
        file=self.entry4.get()
        if(file[-4:].lower()==".lgr"):
            print("Unzipped files will be stored in LGR_Utility.exe's folder.\nOpening 1 lgr file: %s"%(file))
            unzip_LGR(file,default_list)
        elif(file[-4:].lower()==".txt"):
            return
        else:
            print("Unzipped files will be stored in LGR_Utility.exe's folder.\nOpening folder %s and processing all .lgr files"%(file))
            for lgrf in os.listdir(file):
                if(lgrf.lower().endswith(".lgr")):
                    print("Processing: %s"%(lgrf))
                    unzip_LGR(file+"/"+lgrf,default_list)
        print("Done!\n\n\n")
        return
    def CloseStuff(self):
        file=self.entry4.get()
        if(file[-4:].lower()==".txt"):
            zip_LGR(file)
        return
root=tk.Tk()
root.title("sunl's LGR Utility")
app=Application(master=root)

app.mainloop()